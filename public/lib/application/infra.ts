export type Infra = Readonly<{
    host: HostConfig
}>

export type HostConfig = Readonly<{
    secureServerHost: string
}>
