export interface AuthHref {
    passwordLoginHref(): string
    passwordResetSessionHref(): string
}
