import { HomeHref } from "./home/href"

export type AppHref = Readonly<{
    home: HomeHref
}>
