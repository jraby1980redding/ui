export interface HomeHref {
    dashboardHref(): string
    documentsHref(): string
}
