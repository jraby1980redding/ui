# CHANGELOG

## Version : 0.6.0

- fix: check status : See merge request getto-systems-base/projects/example/ui!283


## Version : 0.5.3

- fix: gitlab-ci : See merge request getto-systems-base/projects/example/ui!281



## Version : 0.5.2



## Version : 0.5.1



## Version : 0.5.0

- use: protocol buffers : See merge request getto-systems-base/projects/example/ui!76


## Version : 0.4.0

- fix: auth_client : See merge request getto-systems-base/projects/example/ui!74
- fix: bump ignore : See merge request getto-systems-base/projects/example/ui!73


## Version : 0.3.5

- use: protocol buffers : See merge request getto-systems-base/projects/example/ui!71
- add: protocol buffers files : See merge request getto-systems-base/projects/example/ui!70
- fix: nonce factory method : See merge request getto-systems-base/projects/example/ui!69
- refactor: credential data : See merge request getto-systems-base/projects/example/ui!68
- refactor: var name : See merge request getto-systems-base/projects/example/ui!67
- use: protocol buffers in local storage : See merge request getto-systems-base/projects/example/ui!66


## Version : 0.3.4

- fix: deploy : See merge request getto-systems-base/projects/example/ui!64


## Version : 0.3.3

- fix: root index : See merge request getto-systems-base/projects/example/ui!62


## Version : 0.3.2

- fix: metadata : See merge request getto-systems-base/projects/example/ui!60


## Version : 0.3.1

- fix: prepare : See merge request getto-systems-base/projects/example/ui!58


## Version : 0.3.0

- fix: deploy : See merge request getto-systems-base/projects/example/ui!56
- fix: auth_client : See merge request getto-systems-base/projects/example/ui!55
- refactor: auth_client : See merge request getto-systems-base/projects/example/ui!54
- refactor: auth_client : See merge request getto-systems-base/projects/example/ui!53
- refactor: auth_client : See merge request getto-systems-base/projects/example/ui!52
- refactor: auth_client : See merge request getto-systems-base/projects/example/ui!51
- fix: import order : See merge request getto-systems-base/projects/example/ui!50
- refactor: auth_client : See merge request getto-systems-base/projects/example/ui!49
- rename: y_static : See merge request getto-systems-base/projects/example/ui!48
- update: package : See merge request getto-systems-base/projects/example/ui!47
- fix: parse api roles : See merge request getto-systems-base/projects/example/ui!46


## Version : 0.2.0

- fix: bump ignore : See merge request getto-systems-base/projects/example/ui!44
- fix: fetch : See merge request getto-systems-base/projects/example/ui!42
- fix: renew error : See merge request getto-systems-base/projects/example/ui!41
- feature: password login : See merge request getto-systems-base/projects/example/ui!40
- update: typescript : See merge request getto-systems-base/projects/example/ui!39
- add: credential storage impl : See merge request getto-systems-base/projects/example/ui!38
- fix: css version : See merge request getto-systems-base/projects/example/ui!37
- support: delayed : See merge request getto-systems-base/projects/example/ui!36
- fix: password login : See merge request getto-systems-base/projects/example/ui!35
- feature: password login : See merge request getto-systems-base/projects/example/ui!34
- fix: import order : See merge request getto-systems-base/projects/example/ui!33
- fix: load : See merge request getto-systems-base/projects/example/ui!32
- fix: password input : See merge request getto-systems-base/projects/example/ui!31
- add: password_login : See merge request getto-systems-base/projects/example/ui!30
- refactor: credential : See merge request getto-systems-base/projects/example/ui!29
- add: password-login skelton : See merge request getto-systems-base/projects/example/ui!28
- initialize: preact : See merge request getto-systems-base/projects/example/ui!27
- merge: docs : See merge request getto-systems-base/projects/example/ui!26


## Version : 0.1.10

- fix: gitlab ci : See merge request getto-systems-base/projects/example/ui!24
- version bumped: 0.1.9 : See merge request getto-systems-base/projects/example/ui!23
- fix: gitlab ci : See merge request getto-systems-base/projects/example/ui!22


## Version : 0.1.9

- fix: gitlab ci : See merge request getto-systems-base/projects/example/ui!22


## Version : 0.1.8

- fix: prepare : See merge request getto-systems-base/labo/project-example/ui!20



## Version : 0.1.7

- fix: deploy.sh : See merge request getto-systems-base/labo/project-example/ui!18



## Version : 0.1.6

- fix: deploy.sh : See merge request getto-systems-base/labo/project-example/ui!16

- fix: deploy.sh : See merge request getto-systems-base/labo/project-example/ui!15


## Version : 0.1.5

- fix: deploy.sh : See merge request getto-systems-base/labo/project-example/ui!13


## Version : 0.1.4

- fix: root path : See merge request getto-systems-base/labo/project-example/ui!11


## Version : 0.1.3

- fix: deploy.sh : See merge request getto-systems-base/labo/project-example/ui!9


## Version : 0.1.2

- fix: webpack config : See merge request getto-systems-base/labo/project-example/ui!7


## Version : 0.1.1

- fix: gitlab-ci : See merge request getto-systems-base/labo/project-example/ui!5


## Version : 0.1.0


